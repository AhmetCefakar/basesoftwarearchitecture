﻿using BaseSoftwareArchitecture.BusinessLocigLayer.Abstract;
using BaseSoftwareArchitecture.Core.CrossCuttingConcern.Security.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BaseSoftwareArchitecture.UI.WebMvc.Controllers
{
	public class AccountController : Controller
	{
		private IUserService _userService;
		public AccountController(IUserService userService)
		{
			_userService = userService;
		}

		// GET: Login işleminin 
		public string Login(string UserName, string Password)
		{
			var user = _userService.GetByUserNameAndPassword(UserName, Password);

			if (user != null)
			{
				AuthenticationHelper.CreateAuthhenticationCookie(
						new Guid(),
						user.UserName,
						user.Email,
						DateTime.Now.AddMinutes(60),
						_userService.GetUserRoles(user).Select(q => q.RoleName).ToArray(), //new string[] { "Admin" },
						false,
						user.FirstName,
						user.LastName);

				return "User is Authentication!";
			}

			return "User is Not Authentication!";
		}
	}
}