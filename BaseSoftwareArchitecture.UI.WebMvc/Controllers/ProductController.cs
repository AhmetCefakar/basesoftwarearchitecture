﻿using BaseSoftwareArchitecture.BusinessLocigLayer.Abstract;
using BaseSoftwareArchitecture.Entity.Concrete.Northwind.BaseType;
using BaseSoftwareArchitecture.UI.WebMvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BaseSoftwareArchitecture.UI.WebMvc.Controllers
{
	public class ProductController : Controller
	{
		// Burada istenen 'IProductService' türünün çözümlemesi BLL katmanında IOC tool ile tanımlanıyor.
		private IProductService _productManager;
		public ProductController(IProductService productManager)
		{
			_productManager = productManager;
		}

		// GET: Product
		public ActionResult Index()
		{
			var model = new ProductListViewModel
			{
				Products = _productManager.GetAll()
			};

			return View(model);
		}

		public string Add()
		{
			_productManager.Add(new Products
			{
				CategoryID = 3,
				ProductName = "I added new Product value!",
				QuantityPerUnit = "Sample New-X!",
				UnitPrice = 21
			});

			return "Added!";
		}

		// Transection'ı test etmek  için kullanılan action
		// Update işleminde alınabilecek hatayla ilgili: https://haydarsahin.com/attaching-an-entity-of-type-entity-adi-failed-because-another-entity-of-the-same-type-already-has-the-same-primary-key-value-this-can-happen-when-using-the-attach-method-or-setting-the-state/
		public string Update()
		{
			Products productsUpdate = _productManager.GetById(79);
			productsUpdate.ProductName = "79 Id yeni X-X";
			productsUpdate.UnitPrice = 37;

			_productManager.TransactionalOperation(
				new Products
				{
					CategoryID = 1,
					ProductName = "Example New-7!",
					QuantityPerUnit = "Samxxxple New-2!",
					UnitPrice = 18
				}, productsUpdate);

			return "Transection Done!";

		}

	}
}