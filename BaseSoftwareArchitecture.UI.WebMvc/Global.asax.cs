﻿using BaseSoftwareArchitecture.BusinessLocigLayer.DependencyResolvers.Ninject;
using BaseSoftwareArchitecture.Core.CrossCuttingConcern.Security.Web;
using BaseSoftwareArchitecture.UI.WebMvc.Utilities.Mcv.InfraStructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;

namespace BaseSoftwareArchitecture.UI.WebMvc
{
	public class MvcApplication : System.Web.HttpApplication
	{
		protected void Application_Start()
		{
			AreaRegistration.RegisterAllAreas();
			RouteConfig.RegisterRoutes(RouteTable.Routes);

			// Varsayılan Controller'ın değişmesiyle ilgili eklenen kod.
			ControllerBuilder.Current.SetControllerFactory(new NinjectControllerFactory(
				new BusinessModule(),
				new ValidationModule(),
				new AutoMapperModule()));
		}


		#region Authentication işlemleri için eklenenler 
		public override void Init()
		{
			PostAuthenticateRequest += MvcApplication_PostAuthenticateRequest;
			base.Init();
		}

		// Bu metod yapılan her Request isteğinde çalışacaktır. 
		// Bu şekilde '.Net MVC UI' tarafında gelen Requestler bu metod ile kontorl ediliyor.
		private void MvcApplication_PostAuthenticateRequest(object sender, EventArgs e)
		{
			try
			{
				var authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
				if (authCookie == null)
				{
					return;
				}

				var encTicket = authCookie.Value;
				if (string.IsNullOrEmpty(encTicket))
				{
					return;
				}

				var ticket = FormsAuthentication.Decrypt(encTicket);

				var securityUtlities = new SecurityUtilities();
				var identity = securityUtlities.FormsAuthenticationToIdentity(ticket);
				var principle = new GenericPrincipal(identity, identity.Roles);

				HttpContext.Current.User = principle;
				Thread.CurrentPrincipal = principle;
			}
			catch (Exception ex)
			{
				//throw;
			}

		}
		#endregion

	}
}
