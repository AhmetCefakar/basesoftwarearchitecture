﻿using BaseSoftwareArchitecture.Entity.Concrete.Northwind.BaseType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseSoftwareArchitecture.UI.WebMvc.Models
{
	public class ProductListViewModel
	{
		public List<Products> Products { get; set; }
	}
}