﻿using Ninject;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace BaseSoftwareArchitecture.UI.WebMvc.Utilities.Mcv.InfraStructure
{
	// MVC'de Controller her hangi bir kurucu içeremediği için, kuruculu controller kullanabilmek için eklenen sınıf
	public class NinjectControllerFactory : DefaultControllerFactory
	{
		private IKernel _kernel;
		public NinjectControllerFactory(params INinjectModule[] modules)
		{
			_kernel = new StandardKernel(modules);
		}

		protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
		{
			return controllerType == null ? null : (IController)_kernel.Get(controllerType);
		}
	}
}