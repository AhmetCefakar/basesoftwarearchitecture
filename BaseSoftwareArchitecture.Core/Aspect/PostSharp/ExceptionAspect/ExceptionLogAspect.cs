﻿using BaseSoftwareArchitecture.Core.CrossCuttingConcern.Logging.Log4Net;
using PostSharp.Aspects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace BaseSoftwareArchitecture.Core.Aspect.PostSharp.ExceptionAspect
{
	// Uygulamada hata alınınca log atılmasını sağlayan aspect. Todo: Bu kısım 'LogAspect' ile birleştirilebilir!
	[Serializable]
	public class ExceptionLogAspect : OnExceptionAspect
	{
		[NonSerialized]
		private LoggerService _loggerService;
		private Type _loggerType;
		public ExceptionLogAspect(Type loggerType)
		{
			_loggerType = loggerType;
		}

		// Runtime anında gelen Type değerine göre 'LoggerService' nesnesi üreten metod
		public override void RuntimeInitialize(MethodBase method)
		{
			if (_loggerType.BaseType != typeof(LoggerService)) // Gönderilen 'loggerType' değeri 'LoggerService' türünden mi diye kontrol ediliyor.
			{
				throw new Exception("Wrong Logger Type!");
			}

			_loggerService = (LoggerService)Activator.CreateInstance(_loggerType);
			base.RuntimeInitialize(method);
		}
		
		// Hata alınınca işletilecek olan metod
		public override void OnException(MethodExecutionArgs args)
		{
			if (_loggerService != null)
			{
				_loggerService.LogError(args.Exception);
			}
		}
	}
}
