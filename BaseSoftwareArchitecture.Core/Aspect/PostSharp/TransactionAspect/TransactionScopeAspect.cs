﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using PostSharp.Aspects;

namespace BaseSoftwareArchitecture.Core.Aspect.PostSharp.TransactionAspect
{
	[Serializable]
	public class TransactionScopeAspect : OnMethodBoundaryAspect
	{
		// '_option' transection işleminin kullanımını özel parametreler ile yapmak için kullanılan değişken.
		private TransactionScopeOption _option;
		public TransactionScopeAspect(TransactionScopeOption option)
		{
			_option = option;
		}

		public TransactionScopeAspect()
		{
		}

		// 'TransactionScopeAspect' attribute'ü kullanılan metodun başında çalıştırılan kod.
		public override void OnEntry(MethodExecutionArgs args)
		{
			args.MethodExecutionTag = new TransactionScope(_option);
		}

		// 'TransactionScopeAspect' attribute'ü kullanılan metodunun içinde yapılan işlemler sorunsuzsa bu kod çalışır.
		public override void OnSuccess(MethodExecutionArgs args)
		{
			((TransactionScope)args.MethodExecutionTag).Complete();
		}

		// 'TransactionScopeAspect' attribute'ü kullanılan metod'ta hata oluşursa bu kod çalışır.
		public override void OnExit(MethodExecutionArgs args)
		{
			((TransactionScope)args.MethodExecutionTag).Dispose();
		}

	}
}
