﻿using BaseSoftwareArchitecture.Core.CrossCuttingConcern.Caching;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using PostSharp.Aspects;

namespace BaseSoftwareArchitecture.Core.Aspect.PostSharp.CacheAspect
{
	/// <summary>
	/// 'Add, Update, Delete' vb. işlemlerden sonra ilgili veritabanı değerleri değişir ve cache verilerinin silinmesi gerekir,
	/// bu Aspect bu gibi durumlarda tercih edilecek.
	/// </summary>
	[Serializable]
	public class MemoryCacheRemoveAspect: OnMethodBoundaryAspect
	{
		private string _expression;
		private Type _cacheType;
		private ICacheManager _cacheManager;

		public MemoryCacheRemoveAspect(Type cacheType)
		{
			_cacheType = cacheType;
		}

		public MemoryCacheRemoveAspect(Type cacheType, string expression)
		{
			_cacheType = cacheType;
			_expression = expression;
		}

		// Gönderilen tipte Cache sınıfının runtime anında üretilmesini sağlayan metod
		public override void RuntimeInitialize(MethodBase method)
		{
			if (typeof(ICacheManager).IsAssignableFrom(_cacheType) == false)
			{
				throw new Exception("Wrong Cache Manager");
			}

			_cacheManager = (ICacheManager)Activator.CreateInstance(_cacheType);
			base.RuntimeInitialize(method);
		}

		// İlgili işlem başarılı olduğunda girilecek motod. Bu metoda girilir ve ilgili key değerindeki Cache verileri silinir
		public override void OnSuccess(MethodExecutionArgs args)
		{
			_cacheManager.RemoveByExpression(
				string.IsNullOrEmpty(_expression) 
				? string.Format("{0}.{1}.*", args.Method.ReflectedType.Namespace, args.Method.ReflectedType.Name) 
				: _expression);

			base.OnSuccess(args);
		}
	}
}
