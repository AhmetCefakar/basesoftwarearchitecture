﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using BaseSoftwareArchitecture.Core.CrossCuttingConcern.Caching;
using PostSharp.Aspects;

namespace BaseSoftwareArchitecture.Core.Aspect.PostSharp.CacheAspect
{
	/// <summary>
	/// Cache'e eklenmek istenen 'GetAll' vb. metodlarda kullanılacak Aspect
	/// </summary>
	[Serializable]
	public class MemoryCacheAspect : MethodInterceptionAspect
	{
		public Type _cacheType;
		public int _cacheByMinute;
		public ICacheManager _cacheManager { get; set; }

		public MemoryCacheAspect(Type cacheType, int cacheByMinute = 60)
		{
			_cacheType = cacheType;
			_cacheByMinute = cacheByMinute;
		}

		// Gönderilen tipte Cache sınıfının runtime anında üretilmesini sağlayan metod
		public override void RuntimeInitialize(MethodBase method)
		{
			if (typeof(ICacheManager).IsAssignableFrom(_cacheType) == false)
			{
				throw new Exception("Wrong Cache Manager");
			}

			_cacheManager = (ICacheManager)Activator.CreateInstance(_cacheType);
			base.RuntimeInitialize(method);
		}

		// Metod çağırıldığında çalıştırılacak metod
		public override void OnInvoke(MethodInterceptionArgs args)
		{
			// Kullanılan metoda göre cache anahtarının üretilmesi
			var methodName = string.Format("{0}.{1}.{2}",
				args.Method.ReflectedType.Namespace,
				args.Method.ReflectedType.Name,
				args.Method.Name);

			var arguments = args.Arguments.ToList();

			// Tutulacak olan cache'in anahtarının oluşturulması, burada anahtar metoda yollanan parametreler varsa onlarda dahil edilerek oluşturuluyor
			var key = string.Format("{0}.({1})", methodName,
				string.Join(",", arguments.Select(q => q != null ? q.ToString() : "<Null>")));

			// Cache'te ilgili anahtarın olup olmadığına göre yapılan işlemler
			if (_cacheManager.IsAdd(key))
			{
				args.ReturnValue = _cacheManager.Get<object>(key); // Değer cache'ten getiriliyor
			}
			else
			{
				_cacheManager.Add(key, args.ReturnValue, _cacheByMinute); // 
				base.OnInvoke(args);
			}

		}

	}
}
