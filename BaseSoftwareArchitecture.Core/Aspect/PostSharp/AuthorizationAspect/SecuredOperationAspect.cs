﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PostSharp.Aspects;

// Security için eklenen .Net namespace değerleri
using System.Threading;
using System.Security;


namespace BaseSoftwareArchitecture.Core.Aspect.PostSharp.AuthorizationAspect
{
	/// <summary>
	/// Güvenlik için kullanılan Aspect sınıfı, bu sınıf kullanıldığı metoda girerken sadece contsructor'una verilen 
	/// rollerdeki kullanıcıların girişine izin vermeyi diğğer kullanıcıların engellenmesini sağlıyor.
	/// </summary>
	[Serializable]
	public class SecuredOperationAspect : OnMethodBoundaryAspect
	{
		public string Roles { get; set; }
		
		/// <summary>
		/// Metoda girmeden önce attribute olarak eklendiği metodun başına içerdiği kodları ekleyen metod.
		/// </summary>
		/// <param name="args"></param>
		public override void OnEntry(MethodExecutionArgs args)
		{
			bool isAuthorized = false;
			string[] roles = Roles.Split(',').ToArray();

			for (int i = 0; i < roles.Length; i++)
			{
				// Burada kullanılan 'Thread.CurrentPrincipal' yapısı .Net standart kütüphanesiyle gelen 
				// güvenlik ile ilgili bir yapı ve identity mekanizmasını dll tarafından kullanılmasını sağlıyor
				bool kontroledilenDeger = Thread.CurrentPrincipal.IsInRole(roles[i]);
				if (kontroledilenDeger)
				{
					isAuthorized = true;
				}
			}

			if (isAuthorized == false)
			{
				throw new SecurityException("You are not Authoried");
			}
		}

	}
}
