﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using BaseSoftwareArchitecture.Core.CrossCuttingConcern.Logging;
using BaseSoftwareArchitecture.Core.CrossCuttingConcern.Logging.Log4Net;
using PostSharp.Aspects;
using PostSharp.Extensibility;

namespace BaseSoftwareArchitecture.Core.Aspect.PostSharp.LogAspect
{
	[Serializable]
	[MulticastAttributeUsage(MulticastTargets.Method, TargetMemberAttributes = MulticastAttributes.Instance)] // Bu aspect'in constructior dışındaki metodlara uygulamasını sağlayan attribute tanımı
	public class LogAspect : OnMethodBoundaryAspect
	{
		[NonSerialized]
		private LoggerService _loggerService;
		private Type _loggerType;
		public LogAspect(Type loggerType)
		{
			_loggerType = loggerType;
		}

		// Runtime anında gelen Type değerine göre 'LoggerService' nesnesi üreten metod
		public override void RuntimeInitialize(MethodBase method)
		{
			if (_loggerType.BaseType != typeof(LoggerService)) // Gönderilen 'loggerType' değeri 'LoggerService' türünden mi diye kontrol ediliyor.
			{
				throw new Exception("Wrong Logger Type!");
			}

			_loggerService = (LoggerService)Activator.CreateInstance(_loggerType);
			base.RuntimeInitialize(method);
		}

		// Herhangi bir metoda girilince bu 'kim, ne zaman, hangi verilerle' kullandı bilgisini tutmak için yazılan işlemler.
		public override void OnEntry(MethodExecutionArgs args)
		{
			if (_loggerService.IsInfoEnable)
			{
				try
				{
					// Loglanacak Info verisinin oluşturulup loglanması işlemi
					var logParameters = args.Method.GetParameters().Select(
						(t, i) => new LogMethodParameter // t: Type değeri, i: Iterasyon değeri(0, 1, 2 ...) 
					{
							Name = t.Name,
							Type = t.ParameterType.Name,
							Value = args.Arguments.GetArgument(i)
						}).ToList();

					var logDetail = new LogMethodDetail
					{
						FullName = args.Method.DeclaringType?.FullName, // args.Method.DeclaringType == null ? null : args.Method.DeclaringType.Name,
						MethodName = args.Method.Name,
						LogParameterList = logParameters
					};

					_loggerService.LogInfo(logDetail);
				}
				catch (Exception ex)
				{
					throw ex;
				}
			}
		}

	}
}
