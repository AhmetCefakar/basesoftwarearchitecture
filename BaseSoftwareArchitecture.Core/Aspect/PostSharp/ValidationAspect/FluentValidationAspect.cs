﻿using BaseSoftwareArchitecture.Core.CrossCuttingConcern.Validation.FluentValidation;
using FluentValidation;
using PostSharp.Aspects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSoftwareArchitecture.Core.Aspect.PostSharp.ValidationAspect
{
	[Serializable] // Bir 'Aspct'  'Serializable' tanımlanmalı ki kod'a compile anında ilgili aspect kodları zerkedilebilsin.
	public class FluentValidationAspect : OnMethodBoundaryAspect
	{
		// Dependency Injection.  
		private Type _validatorType;
		public FluentValidationAspect(Type validatorType)
		{
			_validatorType = validatorType;
		}

		// Metoda ilk girişte eklenecek kodların tanımlandığı metod.
		public override void OnEntry(MethodExecutionArgs args)
		{
			IValidator validator = (IValidator)Activator.CreateInstance(_validatorType); // burada dışarıdan gelen validation sınıfının new'lenmiş hali üretiliyor.
			Type entityType = _validatorType.BaseType.GetGenericArguments()[0]; // Validator'ın çalıştığı entity sınıfının elde edilmesi, şu hali ile tek bir model için çalışıyor.
			var entities = args.Arguments.Where(type => type.GetType() == entityType);

			foreach (var entity in entities)
			{
				ValidatorTool.FluentValidate(validator, entity);
			}
		}
	}
}
