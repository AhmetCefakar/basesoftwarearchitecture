﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSoftwareArchitecture.Core.CrossCuttingConcern.Validation.FluentValidation
{
	// Uygulamada kullanılan validation işlemlerinin ortak olarak yönetildiği sınıf
	public class ValidatorTool
	{
		/// <summary>
		/// Validasyon için kullanılacak olan metod.
		/// Bu metod 'Validation AOP' olarak tasarlanan yapı tarafından(burada FluentValidationAspect sınıfı) kullanılıyor.
		/// </summary>
		/// <param name="validator">Doğrulama için yazılmış olan sınıf(Genelde 'Fluent Validation' yaklaşımı kullanılır)</param>
		/// <param name="entity">Doğrulanacak olan sınıf(Genelde Entity sınıfları olur)</param>
		public static void FluentValidate(IValidator validator, object entity)
		{
			var result = validator.Validate(entity);
			
			if (result.Errors.Count > 0)
			{
				throw new ValidationException(result.Errors);
			}
		}
	}
}
