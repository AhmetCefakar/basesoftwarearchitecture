﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Caching;
using System.Text.RegularExpressions;

namespace BaseSoftwareArchitecture.Core.CrossCuttingConcern.Caching.MicrosoftMemoryCache
{
	// Bu sınıfta Microsoft'un varsayılan memoryCache teknolojisi kullanılıyor
	public class MemoryCacheManager : ICacheManager
	{
		protected ObjectCache Cache
		{
			get
			{
				return MemoryCache.Default;
			}
		}

		public T Get<T>(string key)
		{
			return (T)Cache[key];
		}

		public void Add(string key, object data, int cachetime = 60)
		{
			if (data == null)
			{
				return;
			}

			var policy = new CacheItemPolicy { AbsoluteExpiration = DateTime.Now + TimeSpan.FromMinutes(cachetime) };
			Cache.Add(new CacheItem(key, data), policy);
		}

		public bool IsAdd(string key)
		{
			return Cache.Contains(key);
		}

		public void Remove(string key)
		{
			Cache.Remove(key);
		}

		public void RemoveByExpression(string pattern)
		{
			Regex regex = new Regex(pattern, RegexOptions.Singleline | RegexOptions.Compiled | RegexOptions.IgnoreCase);
			var keysToRemove = Cache.Where(q => regex.IsMatch(q.Key)).Select(q => q.Key).ToList();

			foreach (var item in keysToRemove)
			{
				Remove(item);
			}
		}
		
		public void Clear()
		{
			foreach (var item in Cache)
			{
				Remove(item.Key);
			}
		}
	}
}
