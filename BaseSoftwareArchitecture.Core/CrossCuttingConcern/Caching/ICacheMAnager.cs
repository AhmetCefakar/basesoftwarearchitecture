﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSoftwareArchitecture.Core.CrossCuttingConcern.Caching
{
	// Cache işleminin şablonunu belirlemek için kullanılan interface
	public interface ICacheManager
	{
		T Get<T>(string key); // Her bir cache'in bir eşsiz key değeri olacak
		void Add(string key, object data, int cachetime);
		bool IsAdd(string key); // Key değerinde cache var mı
		void Remove(string key); // Key değerindeki cache'i sil
		void RemoveByExpression(string pattern); // Verilen 'Expression' değerine göre cache'i sil
		void Clear(); // Bütün cache'i temizle
	}
}
