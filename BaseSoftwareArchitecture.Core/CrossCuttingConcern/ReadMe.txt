﻿'CrossCuttingConcern' klasörü altında herhangi bir projede kullanıla bilecek olan 'Validation, Transaction, Sequrity, Logging vb.'
işlemler bulunuyor bu şekilde bu 'Core' projesini bir kere yazıdıktan sonra her projemizde kullanabiliriz.
Not: İlgili 'Cross Cutting Consorn' yapılarının varsa proje nesneleri ile doğrudan bağlantılı kısımları bunlar projede 'BLL' katmanına yerleştirilir. 
Mesela validation işlemlerinde her bir entity sınıfı için yazılacak olan kuralları belirten dosyalar BLL katmanında tutulur çünkü entity nesneleri ilgili projeye özgü 
varlıklardır bunlar 'Core' katmanına yazılmaz.