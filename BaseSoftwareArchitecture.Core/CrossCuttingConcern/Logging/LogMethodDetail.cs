﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSoftwareArchitecture.Core.CrossCuttingConcern.Logging
{
	/// <summary>
	/// Uygulamadaki hangi metodun kullanıldığı bilgisini tutmak için kullanılan Log sınıfı.
	/// FullName : Metod'un bulunduğu sınıfın namespace olarak tam yolunun bilgisi
	/// MethodName : Metod'un adı 
	/// List<LogParameter> : Metodun aldığı parametrelerin listesi
	/// </summary>
	public class LogMethodDetail
	{
		public string FullName { get; set; }
		public string MethodName { get; set; }
		public List<LogMethodParameter> LogParameterList { get; set; }
	}
}
