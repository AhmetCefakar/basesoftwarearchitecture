﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSoftwareArchitecture.Core.CrossCuttingConcern.Logging
{
	/// <summary>
	/// Loglanacak Method'un aldığı her bir parametrenin bilgisini taşıyacak olan sınıf.
	/// Type: Metod parametresinin tipi
	/// Name: Metod parametresinin adı
	/// Value: Metod parametresinin değeri
	/// </summary>
	public class LogMethodParameter
	{
		public string Type { get; set; }
		public string Name { get; set; }
		public object Value { get; set; }
	}
}
