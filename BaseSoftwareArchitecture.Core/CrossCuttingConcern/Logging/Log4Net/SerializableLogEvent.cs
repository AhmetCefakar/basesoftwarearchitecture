﻿using log4net.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSoftwareArchitecture.Core.CrossCuttingConcern.Logging.Log4Net
{
	// Loglar json formatında tutulduğu için bu sınıf 'Serializable' olrak tanımlandı
	[Serializable]
	public class SerializableLogEvent
	{
		private LoggingEvent _loggingEvent;
		public SerializableLogEvent(LoggingEvent loggingEvent)
		{
			_loggingEvent = loggingEvent;
		}

		public string UserName
		{
			get
			{
				return _loggingEvent.UserName;
			}
		}

		public object MessageObject
		{
			get
			{
				return _loggingEvent.MessageObject;
			}
		}

	}
}
