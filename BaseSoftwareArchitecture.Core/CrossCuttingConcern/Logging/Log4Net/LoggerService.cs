﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSoftwareArchitecture.Core.CrossCuttingConcern.Logging.Log4Net
{
	/// <summary>
	/// Lg4Net ile 5 tür altındaki(Info, Debug, Warn, Error, Fatal) logların işlenmesi
	/// </summary>
	[Serializable]
	public class LoggerService
	{
		private ILog _log;
		public LoggerService(ILog log)
		{
			_log = log;
		}

		public bool IsInfoEnable
		{
			get
			{
				return _log.IsInfoEnabled;
			}
		}

		public bool IsDebugEnabled
		{
			get
			{
				return _log.IsDebugEnabled;
			}
		}

		public bool IsWarnEnabled
		{
			get
			{
				return _log.IsWarnEnabled;
			}
		}

		public bool IsErrorEnabled
		{
			get
			{
				return _log.IsErrorEnabled;
			}
		}

		public bool IsFatalEnabled
		{
			get
			{
				return _log.IsFatalEnabled;
			}
		}


		public void LogInfo(object logMessage)
		{
			if (IsInfoEnable)
			{
				_log.Info(logMessage);
			}
		}

		public void LogWarn(object logMessage)
		{
			if (IsWarnEnabled)
			{
				_log.Warn(logMessage);
			}
		}

		public void LogDebug(object logMessage)
		{
			if (IsDebugEnabled)
			{
				_log.Debug(logMessage);
			}
		}

		public void LogError(object logMessage)
		{
			if (IsErrorEnabled)
			{
				_log.Error(logMessage);
			}
		}

		public void LogFatal(object logMessage)
		{
			if (IsFatalEnabled)
			{
				_log.Fatal(logMessage);
			}
		}
	}
}
