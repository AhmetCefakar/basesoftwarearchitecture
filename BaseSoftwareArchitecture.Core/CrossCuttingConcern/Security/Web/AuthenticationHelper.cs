﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Security;

namespace BaseSoftwareArchitecture.Core.CrossCuttingConcern.Security.Web
{
	// Web uygulamalarında kullanabilmek için Helper, Bu sınıf kullanıcı bilgileri alıyor şifreliyor ve cookie'ye ekliyor
	public class AuthenticationHelper
	{
		public static void CreateAuthhenticationCookie(
			Guid id,
			string userName,
			string email,
			DateTime expriration,
			string[] roles,
			bool rememberMe,
			string firstName,
			string lastName)
		{
			FormsAuthenticationTicket authentication = new FormsAuthenticationTicket(1, userName, DateTime.Now, expriration, rememberMe,
				CreateAuthhenticationTag(id, email, firstName, lastName, roles));
			string encryptTicket = FormsAuthentication.Encrypt(authentication); // 'FormsAuthenticationTicket' sınıfındaki user biletinin şifrelenmesi

			HttpContext.Current.Response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, encryptTicket)); // Şİfrelenen kullanıcı bileti Cookie'ye eklenmiş oluyor

		}

		// FormsAuthenticationTicket yapısında her bir kullanıcı için tutulacak olan kullanıcının bilgilerini string formatında oluşturmayı sağlayan metod.
		private static string CreateAuthhenticationTag(Guid id, string email, string firstName, string lastName, string[] roles)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(id);
			stringBuilder.Append("|");
			stringBuilder.Append(email);
			stringBuilder.Append("|");
			stringBuilder.Append(firstName);
			stringBuilder.Append("|");
			stringBuilder.Append(lastName);
			stringBuilder.Append("|");

			for (int i = 0; i < roles.Length; i++)
			{
				stringBuilder.Append(roles[i]);
				if (i < roles.Length -1)
				{
					stringBuilder.Append(",");
				}
			}
			return stringBuilder.ToString();
		}
	}
}
