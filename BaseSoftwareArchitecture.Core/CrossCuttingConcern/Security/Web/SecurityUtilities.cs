﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;

namespace BaseSoftwareArchitecture.Core.CrossCuttingConcern.Security.Web
{
	public class SecurityUtilities
	{
		/// <summary>
		/// 'formsAuthenticationTicket' değerindeki şifreli cookie değerini çözme işlemini yapıp geriye ilgili kullanıcı için değerleri açık şekilde döndüren metod
		/// </summary>
		/// <param name="formsAuthenticationTicket"></param>
		/// <returns></returns>
		public Identity FormsAuthenticationToIdentity(FormsAuthenticationTicket formsAuthenticationTicket)
		{
			var idenntity = new Identity
			{
				Name = GetName(formsAuthenticationTicket),
				AuthenticationType = GetAuthenticationType(),
				IsAuthenticated = GetIsAuthenticated(),
				Id = GetId(formsAuthenticationTicket),
				FirstName = GetFirstName(formsAuthenticationTicket),
				LastName = GetLastName(formsAuthenticationTicket),
				Email = GetEmail(formsAuthenticationTicket),
				Roles = GetRoles(formsAuthenticationTicket),
			};
			
			return idenntity;
		}
		
		#region İlgili 'FormsAuthenticationTicket'ta saklanan cookie değerlerini döndüren metodlar 
		
		private string GetName(FormsAuthenticationTicket formsAuthenticationTicket)
		{
			return formsAuthenticationTicket.Name;
		}

		private string GetAuthenticationType()
		{
			return "Forms";
		}

		private bool GetIsAuthenticated()
		{
			return true;
		}

		
		private Guid GetId(FormsAuthenticationTicket formsAuthenticationTicket)
		{
			string[] data = GetUserData(formsAuthenticationTicket);
			return Guid.Parse(data[0]); // Hangi index'in alınacağı 'AuthenticationHelper' altındaki 'CreateAuthhenticationTag' metoduna göre belirleniyor
		}

		private string GetEmail(FormsAuthenticationTicket formsAuthenticationTicket)
		{
			string[] data = GetUserData(formsAuthenticationTicket);
			return data[1]; // Hangi index'in alınacağı 'AuthenticationHelper' altındaki 'CreateAuthhenticationTag' metoduna göre belirleniyor
		}

		private string GetFirstName(FormsAuthenticationTicket formsAuthenticationTicket)
		{
			string[] data = GetUserData(formsAuthenticationTicket);
			return data[2]; // Hangi index'in alınacağı 'AuthenticationHelper' altındaki 'CreateAuthhenticationTag' metoduna göre belirleniyor 
		}

		private string GetLastName(FormsAuthenticationTicket formsAuthenticationTicket)
		{
			string[] data = GetUserData(formsAuthenticationTicket);
			return data[3]; // Hangi index'in alınacağı 'AuthenticationHelper' altındaki 'CreateAuthhenticationTag' metoduna göre belirleniyor
		}

		private string[] GetRoles(FormsAuthenticationTicket formsAuthenticationTicket)
		{
			string[] data = GetUserData(formsAuthenticationTicket);
			string[] roles = data[4].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries); // Hangi index'in alınacağı 'AuthenticationHelper' altındaki 'CreateAuthhenticationTag' metoduna göre belirleniyor
			return roles;
		}

		private string[] GetUserData(FormsAuthenticationTicket formsAuthenticationTicket)
		{
			string[] userData = formsAuthenticationTicket.UserData.Split('|');
			return userData;
		}

		#endregion
	}
}
