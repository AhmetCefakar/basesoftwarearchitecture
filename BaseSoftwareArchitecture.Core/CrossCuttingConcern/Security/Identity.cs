﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace BaseSoftwareArchitecture.Core.CrossCuttingConcern.Security
{
	public class Identity : IIdentity
	{
		// IIdentity'den gelen property değerleri
		public string Name { get; set; } // Identity Name
		public string AuthenticationType { get; set; } // Form(Kullanıcı bilgilerinin DB benzeri kaynaklarda tutulduğu Authentication türü), windows Authentication vb. gibi 
		public bool IsAuthenticated { get; set; } // 

		// Ayrıca eklenen property'ler
		public Guid Id { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Email { get; set; }
		public string[] Roles { get; set; }

	}
}
