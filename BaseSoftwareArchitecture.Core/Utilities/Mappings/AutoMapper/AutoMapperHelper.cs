﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSoftwareArchitecture.Core.Utilities.Mappings.AutoMapper
{
	// Bu sınıfta tanımlanan metodlar AutoMapper 8.0.0 sürümü ile bir proje içinde sadece 
	// 1 defa 'Mapper.Initialize' kullanımına izin vermesi nedeniyle kullanılabilir değildir!
	public class AutoMapperHelper
	{
		/// <summary>
		/// Mapping processing for same types
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="type"></param>
		/// <returns></returns>
		public static T MapToSameType<T>(T type) where T : class, new()
		{
			Mapper.Initialize(cfg =>
			{
				cfg.CreateMap<T, T>();
			});

			T result = Mapper.Map<T, T>(type);
			return result;
		}

		/// <summary>
		/// Mapping processing for same typesList.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="typeList"></param>
		/// <returns></returns>
		public static List<T> MapToSameTypeList<T>(List<T> typeList) where T : class, new()
		{
			Mapper.Initialize(cfg =>
			{
				cfg.CreateMap<T, T>();
			});

			List<T> resultList = Mapper.Map<List<T>, List<T>>(typeList);
			return resultList;
		}
	}
}
