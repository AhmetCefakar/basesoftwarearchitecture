﻿'Concrete' klasörü altında kullanılan 'Entity' sınıfları turuluyor. 
Daha derli toplu durması için bu klasör altında uygulamada kullanılan veritabanlarının ismiyle klasörler oluşturuldu.
Oluşturulan her bir veritabanı klasörü altında 'BaseType' ve 'ComplexType' adında iki klasör eklendi,
bu klasörlerden 'BaseType' içinde ilgili veritabanındaki tabloların karşılığı olan entity'ler ve 
'ComplexType' klasörü altında da veritabanındaki nesnelere benzemeyen sınıf tipleri yer alacak.