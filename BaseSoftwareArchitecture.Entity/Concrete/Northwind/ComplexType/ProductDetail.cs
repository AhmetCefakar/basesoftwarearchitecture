﻿using BaseSoftwareArchitecture.Entity.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSoftwareArchitecture.Entity.Concrete.Northwind.ComplexType
{
	public class ProductDetail: IEntity
	{
		public virtual int ProductID { get; set; }
		public virtual string ProductName { get; set; }
		public virtual string CategoryName { get; set; }
	}
}
