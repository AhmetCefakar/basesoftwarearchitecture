﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSoftwareArchitecture.Entity.Concrete.Northwind.ComplexType
{
	public class UserRolesComplexType
	{
		public virtual string RoleName { get; set; }
	}
}
