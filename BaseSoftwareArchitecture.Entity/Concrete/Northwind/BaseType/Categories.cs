﻿using BaseSoftwareArchitecture.Entity.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSoftwareArchitecture.Entity.Concrete.Northwind.BaseType
{
	public class Categories : IEntity
	{
		public virtual int CategoryID { get; set; }
		public virtual string CategoryName { get; set; }
	}
}
