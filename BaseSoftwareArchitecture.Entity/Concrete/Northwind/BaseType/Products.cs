﻿using BaseSoftwareArchitecture.Entity.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSoftwareArchitecture.Entity.Concrete.Northwind.BaseType
{
	public class Products : IEntity
	{
		public virtual int ProductID { get; set; }
		public virtual string ProductName { get; set; }
		public virtual int CategoryID { get; set; }
		public virtual string QuantityPerUnit { get; set; }
		public virtual decimal UnitPrice { get; set; }
	}
}
