﻿using BaseSoftwareArchitecture.Entity.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSoftwareArchitecture.Entity.Concrete.Northwind.BaseType
{
	public class UserRole : IEntity
	{
		public virtual int Id { get; set; }
		public virtual int UserId { get; set; }
		public virtual int RoleId { get; set; }
	}
}
