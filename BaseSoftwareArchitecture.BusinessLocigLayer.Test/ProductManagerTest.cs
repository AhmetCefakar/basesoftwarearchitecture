﻿using System;
using AutoMapper;
using BaseSoftwareArchitecture.BusinessLocigLayer.Concrete.Manager;
using BaseSoftwareArchitecture.DataAccessLayer.Abstract.GenericRepository.NorthwindDal.ForOrm;
using BaseSoftwareArchitecture.Entity.Concrete.Northwind.BaseType;
using FluentValidation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace BaseSoftwareArchitecture.BusinessLocigLayer.Test
{
	[TestClass]
	public class ProductManagerTest
	{
		[ExpectedException(typeof(ValidationException))] // Validasyon hatası alınması gerektiği test sınıfına bildiriliyor
		[TestMethod]
		public void Product_Validation_Check()
		{
			// Burada Unit Test için 'IProductDalForOrm' türünden sahte bir conrate nesne oluşturuluyor (Moq dll'i ile)
			Mock<IProductDalForOrm> mockDalFoOrm = new Mock<IProductDalForOrm>();
			Mock<IMapper> mockMapper = new Mock<IMapper>();
			
			ProductManager productManager = new ProductManager(mockDalFoOrm.Object, mockMapper.Object);

			// 'Products' nesnesi için eklenen validation kodlarını alttaki 'Add' metoduna verilen nesne sağlamıyor bundan dolayı hata alınması gerekiyor.
			// bu çok basit bir kullanım misali gerçek bir proje kodlarken bu test metodları baya bir şişer çünkü
			// tek bir metod için birden fazla durumu kontrol etmek istiye biliriz.
			productManager.Add(new Products());
		}
	}
}
