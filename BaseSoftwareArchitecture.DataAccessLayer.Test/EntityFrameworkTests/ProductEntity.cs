﻿using System;
using System.Collections.Generic;
using BaseSoftwareArchitecture.DataAccessLayer.Concrete.ORM.EntityFramework.Context;
using BaseSoftwareArchitecture.DataAccessLayer.Concrete.ORM.EntityFramework.NorthwindDal;
using BaseSoftwareArchitecture.Entity.Concrete.Northwind.BaseType;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BaseSoftwareArchitecture.DataAccessLayer.Test.EntityFrameworkTests
{
	[TestClass]
	public class ProductEntity
	{
		[TestMethod]
		public void Get_All_Products()
		{
			EfProductDal efProductDal = new EfProductDal();

			List<Products> result = efProductDal.GetAll();

			Assert.AreNotEqual(0, result.Count);
		}


		[TestMethod]
		public void Get_All_Products_With_Parameters()
		{
			EfProductDal efProductDal = new EfProductDal();

			List<Products> result = efProductDal.GetList(q => q.ProductName.Contains("ab"));

			Assert.AreNotEqual(0, result.Count);
		}
	}
}
