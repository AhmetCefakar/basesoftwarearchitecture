﻿using System;
using System.Collections.Generic;
using BaseSoftwareArchitecture.DataAccessLayer.Concrete.ORM.NHibernate.Helper;
using BaseSoftwareArchitecture.DataAccessLayer.Concrete.ORM.NHibernate.NorthwindDal;
using BaseSoftwareArchitecture.Entity.Concrete.Northwind.BaseType;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BaseSoftwareArchitecture.DataAccessLayer.Test.NHibernateTests
{
	[TestClass]
	public class ProductEntity
	{
		[TestMethod]
		public void Get_All_Products()
		{
			NhProductDal efProductDal = new NhProductDal(new NorthwindHelper());

			List<Products> result = efProductDal.GetAll();

			Assert.AreNotEqual(0, result.Count);
		}


		[TestMethod]
		public void Get_All_Products_With_Parameters()
		{
			NhProductDal efProductDal = new NhProductDal(new NorthwindHelper());

			List<Products> result = efProductDal.GetList(q => q.ProductName.Contains("ab"));

			Assert.AreNotEqual(0, result.Count);
		}
	}
}
