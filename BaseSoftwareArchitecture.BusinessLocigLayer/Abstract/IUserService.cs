﻿using BaseSoftwareArchitecture.Entity.Concrete.Northwind.BaseType;
using BaseSoftwareArchitecture.Entity.Concrete.Northwind.ComplexType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BaseSoftwareArchitecture.BusinessLocigLayer.Abstract
{
	/// <summary>
	/// ProductManager sınıfının operasyonlarını tutan interface
	/// Bu Interface servis katmanındaki implemantasyon projeleri tarafından kullanılabilir
	/// </summary>
	public interface IUserService
	{
		List<User> GetAll();
		User GetById(int id);
		User Add(User product);
		User Update(User product);

		User GetByUserNameAndPassword(string UsenName, string Password);
		List<UserRolesComplexType> GetUserRoles(User user);
	}
}
