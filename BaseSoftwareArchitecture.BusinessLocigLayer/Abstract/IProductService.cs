﻿using BaseSoftwareArchitecture.Entity.Concrete.Northwind.BaseType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BaseSoftwareArchitecture.BusinessLocigLayer.Abstract
{
	/// <summary>
	/// ProductManager sınıfının operasyonlarını tutan interface
	/// Bu Interface servis katmanındaki implemantasyon projeleri tarafından kullanılabilir
	/// </summary>
	public interface IProductService
	{
		List<Products> GetAll();
		Products GetById(int id);
		Products Add(Products product);
		Products Update(Products product);
		void TransactionalOperation(Products products1, Products products2);
	}
}
