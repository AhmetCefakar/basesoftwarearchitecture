﻿using BaseSoftwareArchitecture.Entity.Concrete.Northwind.BaseType;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSoftwareArchitecture.BusinessLocigLayer.ValidationRules.FluentValidation
{
	/// <summary>
	/// 'Product' entity nesnesi için eklenen valdation sınıfı
	/// </summary>
	class ProductValidator : AbstractValidator<Products>
	{
		public ProductValidator()
		{
			//RuleFor(q => q.ProductID).NotEmpty();
			RuleFor(q => q.ProductName).NotEmpty();
			RuleFor(q => q.UnitPrice).GreaterThan(17).When(q => q.CategoryID == 1);
			RuleFor(q => q.QuantityPerUnit).NotEmpty();
			RuleFor(q => q.ProductName).Length(2, 100);
			//RuleFor(q => q.ProductName).Must(StartWithP); // Özel tanımlanan bir fonksiyon ile kontrol işlemi
		}

		//private bool StartWithP(string value)
		//{
		//	return value.StartsWith("P");
		//}
	}
}
