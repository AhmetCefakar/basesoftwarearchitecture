﻿using AutoMapper;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSoftwareArchitecture.BusinessLocigLayer.DependencyResolvers.Ninject
{
	/// <summary>
	/// Bu sınıf ile AutoMapper'ın BLL katmanında 'Profile' sınıflarını otamatik olarak gezip mapping
	/// işlemine eklemesi sağlanıyor.
	/// Not: Bu sınıf ile 'Core' katmanındaki 'AutoMapperHelper' sınıfının kullanımı gereksiz hale gelmiştir
	/// </summary>
	public class AutoMapperModule : NinjectModule
	{
		public override void Load()
		{
			// Hangi türden sınıf için mapleme yapılacaksa o türden sınıf için işlem yapmayı sağlayan kod
			Bind<IMapper>().ToConstant(CreateConfiguration().CreateMapper()).InSingletonScope();
		}
		
		private MapperConfiguration CreateConfiguration()
		{
			var config = new MapperConfiguration(cfg =>
			{
				// Add all profiles in current assembly(like the 'BusinessProfile' assambly)
				cfg.AddProfiles(GetType().Assembly);
			});

			return config;
		}
	}
}
