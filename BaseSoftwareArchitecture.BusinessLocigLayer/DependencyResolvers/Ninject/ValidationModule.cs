﻿using BaseSoftwareArchitecture.BusinessLocigLayer.ValidationRules.FluentValidation;
using BaseSoftwareArchitecture.Entity.Concrete.Northwind.BaseType;
using FluentValidation;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSoftwareArchitecture.BusinessLocigLayer.DependencyResolvers.Ninject
{
	/// <summary>
	/// Bu sınıf Client-Side validation kullanılmak istendiğinde işe yarayacak(not: sadece .net-MVC vb. için)
	/// </summary>
	public class ValidationModule : NinjectModule
	{
		public override void Load()
		{
			// Artık kaç farklı 'Entity Validation' sınıfı ile çalışılıyorsa buraya eklenmesi gerekir.
			Bind<IValidator<Products>>().To<ProductValidator>().InSingletonScope();
		}
	}
}
