﻿using BaseSoftwareArchitecture.BusinessLocigLayer.Abstract;
using BaseSoftwareArchitecture.BusinessLocigLayer.Concrete.Manager;
using BaseSoftwareArchitecture.DataAccessLayer.Abstract.GenericRepository.NorthwindDal.ForOrm;
using BaseSoftwareArchitecture.DataAccessLayer.Concrete.ORM.EntityFramework.NorthwindDal;
using BaseSoftwareArchitecture.DataAccessLayer.Concrete.ORM.NHibernate.Helper;
using BaseSoftwareArchitecture.DataAccessLayer.Concrete.ORM.NHibernate.NorthwindDal;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSoftwareArchitecture.BusinessLocigLayer.DependencyResolvers.Ninject
{
	/// <summary>
	/// Bu sınıf BLL katmanı için SOLID'in 'D' harfini ifade eden 'Dependency Injection' yapısını uygulamak için projeye eklenmiştir.
	/// Bu sınıfta BLL katmanındaki sınıflara enjekte edilen(genelde sınıfların kurucuları kullanılarak) interface'lerin karşılığında kullanılacak olan sınıflar belirtilir.
	/// Bu metod uygula içerisinde kullanılan sınıfların kurucularından istenen referans türlerinin karşılığında ne tür sınıflar verileceğini tutar.
	/// İstenirse sadece istenen Interface'lerin karşılıkları değil, conctrate sınıfların karşılıkları da tanımlanabilir.
	/// </summary>
	public class BusinessModule : NinjectModule
	{
		public override void Load()
		{
			// DI(Dependency Injection) işlemlerindeki Interface değerlerinin karşılıkları belirtiliyor.(Keyword: IOC Tool)
			Bind<IProductService>().To<ProductManager>().InSingletonScope();
			Bind<IProductDalForOrm>().To<EfProductDal>().InSingletonScope();

			Bind<IUserService>().To<UserManager>().InSingletonScope();
			Bind<IUserDalForOrm>().To<EfUserDal>().InSingletonScope();


			// DAL için kullanılan çözümlemeler(bura düzenlenmesi gerekir 
			// Nh için çünkü bu hali ile sadece tek bir Db ile çalışılabilir)
			Bind<NHibernateHelper>().To<NorthwindHelper>();

		}
	}
}
