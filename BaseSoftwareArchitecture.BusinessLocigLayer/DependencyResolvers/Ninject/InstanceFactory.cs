﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSoftwareArchitecture.BusinessLocigLayer.DependencyResolvers.Ninject
{
	public class InstanceFactory
	{
		/// <summary>
		/// Ninject ve BLL'deki 'BusinessModule' sınıfı kullanılıyor
		/// Kendine belirtilen generic türden instance üretip geriye döndüren metod
		/// </summary>
		/// <typeparam name="T">BusinessModule sınıfında karşılığı olan interface ya da class</typeparam>
		/// <returns></returns>
		public static T GetInstance<T>() where T : class
		{
			var kernel = new StandardKernel(new BusinessModule());
			return kernel.Get<T>();
		}
	}
}
