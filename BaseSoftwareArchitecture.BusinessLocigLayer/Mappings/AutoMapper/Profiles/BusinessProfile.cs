﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AutoMapper;
using AutoMapper.Configuration;
using BaseSoftwareArchitecture.Entity.Concrete.Northwind.BaseType;

namespace BaseSoftwareArchitecture.BusinessLocigLayer.Mappings.AutoMapper.Profiles
{
	public class BusinessProfile : Profile
	{
		public BusinessProfile()
		{
			// Ef'de alınan serialization problemi için tanımlanıyor
			// Eklenecek olan bütün mapping profile tanımlamaları buraya tanımlanır
			CreateMap<Products, Products>();
		}
	}
}
