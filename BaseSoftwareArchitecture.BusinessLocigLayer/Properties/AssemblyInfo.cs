﻿using BaseSoftwareArchitecture.Core.Aspect.PostSharp.ExceptionAspect;
using BaseSoftwareArchitecture.Core.Aspect.PostSharp.LogAspect;
using BaseSoftwareArchitecture.Core.CrossCuttingConcern.Logging.Log4Net.Loggers;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("BaseSoftwareArchitecture.BusinessLocigLayer")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("BaseSoftwareArchitecture.BusinessLocigLayer")]
[assembly: AssemblyCopyright("Copyright ©  2019")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]


// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("9fa78e1a-0488-4df8-97b4-01f0fb6634ab")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]



// Loglama işlemlerinin 'assembly' genelinde yapmka için eklenen kodlar
[assembly: LogAspect(typeof(DatabaseLogger))] // Loglama işlemi Veritabanına 'AOP' yöntemi ile yapılıyor.
[assembly: LogAspect(typeof(FileLogger), AttributeTargetTypes = "BaseSoftwareArchitecture.BusinessLocigLayer.Concrete.Manager.*")] // Loglama işlemi Dosyaya 'AOP' yöntemi ile yapılıyor.(Loglama sadece belirtilen using için yapılacak)
[assembly: ExceptionLogAspect(typeof(DatabaseLogger), AttributeTargetTypes = "BaseSoftwareArchitecture.BusinessLocigLayer.Concrete.Manager.*")] // Exception Loglama işlemi Db'ye 'AOP' yöntemi ile yapılıyor.(Loglama sadece belirtilen using için yapılacak)
