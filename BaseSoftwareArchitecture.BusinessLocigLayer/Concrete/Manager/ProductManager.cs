﻿using BaseSoftwareArchitecture.BusinessLocigLayer.Abstract;
using BaseSoftwareArchitecture.BusinessLocigLayer.ValidationRules.FluentValidation;
using BaseSoftwareArchitecture.DataAccessLayer.Abstract.GenericRepository.NorthwindDal.ForOrm;
using BaseSoftwareArchitecture.Entity.Concrete.Northwind.BaseType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseSoftwareArchitecture.Core.Aspect.PostSharp.ValidationAspect;
using BaseSoftwareArchitecture.Core.Aspect.PostSharp.TransactionAspect;
using BaseSoftwareArchitecture.Core.Aspect.PostSharp.CacheAspect;
using BaseSoftwareArchitecture.Core.CrossCuttingConcern.Caching.MicrosoftMemoryCache;
using BaseSoftwareArchitecture.Core.CrossCuttingConcern.Logging.Log4Net.Loggers;
using BaseSoftwareArchitecture.Core.Aspect.PostSharp.LogAspect;
using BaseSoftwareArchitecture.Core.Aspect.PostSharp.AuthorizationAspect;
using AutoMapper;
using BaseSoftwareArchitecture.Core.Utilities.Mappings.AutoMapper;

namespace BaseSoftwareArchitecture.BusinessLocigLayer.Concrete.Manager
{
	/// <summary>
	/// Todo: BLL katmanında ortak metodlar generic olarak tek metodda toplanabilir
	/// Product ile ilgili BLL işlemlerinin tutulduğu sınıf
	/// </summary>
	//[LogAspect(typeof(DatabaseLogger))] // Loglama işlemi Veritabanına 'AOP' yöntemi ile yapılıyor. 'class' başına yazılınca sınıftaki bütün method çağırımlarında loglama çalıştırılır
	//[LogAspect(typeof(FileLogger))] // Loglama işlemi Dosyaya 'AOP' yöntemi ile yapılıyor.
	public class ProductManager : IProductService
	{
		// Kurucudan değer alarak sınıf içindeki private elemanın değerini atamaya 'Dependecy Injection' deniyor.
		private IProductDalForOrm _productDalForOrm;
		private IMapper _mapper;
		public ProductManager(IProductDalForOrm productDal, IMapper mapper)
		{
			_productDalForOrm = productDal;
			_mapper = mapper;
		}

		//[LogAspect(typeof(DatabaseLogger))] // Kullanılan metodların Veritabanına loglanması işlemi(Bu uzun yöntem 'AssemblyInfo' dosyasından yönetmek daha profesyonel)
		//[LogAspect(typeof(FileLogger))] // Kullanılan metodların Dosyaya loglanması işlemi(Bu uzun yöntem 'AssemblyInfo' dosyasından yönetmek daha profesyonel)
		[MemoryCacheAspect(typeof(MemoryCacheManager))] // Cache işlemi 'AOP' yöntemi ile yapılıyor.
		[SecuredOperationAspect(Roles = "Admin,Editor")] // Rol bazlı güvenlik sağlayan aspect kullanımı.(Burayı sonradan aç)
		public List<Products> GetAll()
		{
			// AutoMapper'ın düz şekilde kullanımı, eğer bu şekilde kullanılırsa her sınıf için ayrı ayrı yazmak gerekecektir.
			// Not: eğer performans mapping işleminde düşerse manuel olarak bu mapping işlemi yapılabilir
			//List<Products> values = _productService.GetAll().Select(q => new Products()
			//{
			//	CategoryID = q.CategoryID,
			//	ProductID = q.ProductID,
			//	ProductName = q.ProductName,
			//	QuantityPerUnit = q.QuantityPerUnit,
			//	UnitPrice = q.UnitPrice
			//}).ToList();

			List<Products> entityList = _mapper.Map<List<Products>>(_productDalForOrm.GetAll()); //AutoMapperHelper.MapToSameTypeList(_productDalForOrm.GetAll());
			return entityList;
		}
		

		public Products GetById(int id)
		{
			return _productDalForOrm.Get(q => q.ProductID.Equals(id));
		}

		[FluentValidationAspect(typeof(ProductValidator))] // Validation işlemi için AOP uygulanmasını sağlayan attribute
		[MemoryCacheRemoveAspect(typeof(MemoryCacheManager))] // Bu işlem çalıştırılınca ilgili Cache değerleri silinecek.
		public Products Add(Products product)
		{
			//ValidatorTool.FluentValidate(new ProductValidatior(), product); // gelen modeli doğrulama yapan satır, bu şekilde her doğrulama yapılacak motoda eklemek gerekiyor ama AOP ile daha temiz çözülebilen bir sorun bu.
			return _productDalForOrm.Add(product);
		}

		[FluentValidationAspect(typeof(ProductValidator))]
		public Products Update(Products product)
		{
			return _productDalForOrm.Update(product);
		}

		// Taransaction Example(Used PostSharp AOP)
		[MemoryCacheRemoveAspect(typeof(MemoryCacheManager))]
		[FluentValidationAspect(typeof(ProductValidator))]
		[TransactionScopeAspect]
		public void TransactionalOperation(Products products1, Products products2)
		{
			_productDalForOrm.Add(products1);
			_productDalForOrm.Update(products2);
			/// business operations...
			//_productDalForOrm.Delete(products2);
		}
	}
}
