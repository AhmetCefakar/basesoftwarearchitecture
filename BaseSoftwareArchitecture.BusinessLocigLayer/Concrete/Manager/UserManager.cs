﻿using BaseSoftwareArchitecture.BusinessLocigLayer.Abstract;
using BaseSoftwareArchitecture.BusinessLocigLayer.ValidationRules.FluentValidation;
using BaseSoftwareArchitecture.DataAccessLayer.Abstract.GenericRepository.NorthwindDal.ForOrm;
using BaseSoftwareArchitecture.Entity.Concrete.Northwind.BaseType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BaseSoftwareArchitecture.Core.Aspect.PostSharp.ValidationAspect;
using BaseSoftwareArchitecture.Core.Aspect.PostSharp.TransactionAspect;
using BaseSoftwareArchitecture.Core.Aspect.PostSharp.CacheAspect;
using BaseSoftwareArchitecture.Core.CrossCuttingConcern.Caching.MicrosoftMemoryCache;
using BaseSoftwareArchitecture.Core.CrossCuttingConcern.Logging.Log4Net.Loggers;
using BaseSoftwareArchitecture.Core.Aspect.PostSharp.LogAspect;
using BaseSoftwareArchitecture.Core.Aspect.PostSharp.AuthorizationAspect;
using BaseSoftwareArchitecture.Entity.Concrete.Northwind.ComplexType;

namespace BaseSoftwareArchitecture.BusinessLocigLayer.Concrete.Manager
{
	/// <summary>
	/// Todo: BLL katmanında ortak metodlar generic olarak tek metodda toplanabilir
	/// user ile ilgili BLL işlemlerinin tutulduğu sınıf
	/// </summary>
	public class UserManager : IUserService
	{
		// Kurucudan değer alarak sınıf içindeki private elemanın değerini atamaya 'Dependecy Injection' deniyor.
		private IUserDalForOrm _userDalForOrm;
		public UserManager(IUserDalForOrm userDal)
		{
			_userDalForOrm = userDal;
		}
		
		[MemoryCacheAspect(typeof(MemoryCacheManager))] // Cache işlemi 'AOP' yöntemi ile yapılıyor.
		[SecuredOperationAspect(Roles = "Admin,Editor")] // Rol bazlı güvenlik sağlayan aspect kullanımı.
		public List<User> GetAll()
		{
			return _userDalForOrm.GetAll();
		}

		public User GetById(int id)
		{
			return _userDalForOrm.Get(q => q.Id.Equals(id));
		}

		//[FluentValidationAspect(typeof(userValidator))] // Validation işlemi için AOP uygulanmasını sağlayan attribute
		[MemoryCacheRemoveAspect(typeof(MemoryCacheManager))] // Bu işlem çalıştırılınca ilgili Cache değerleri silinecek.
		public User Add(User user)
		{
			return _userDalForOrm.Add(user);
		}

		//[FluentValidationAspect(typeof(userValidator))]
		public User Update(User user)
		{
			return _userDalForOrm.Update(user);
		}

		public User GetByUserNameAndPassword(string UsenName, string Password)
		{
			return _userDalForOrm.Get(q => q.UserName == UsenName && q.Password == Password);
		}

		public List<UserRolesComplexType> GetUserRoles(User user)
		{
			return _userDalForOrm.GetUserRoles(user);
		}
	}
}
