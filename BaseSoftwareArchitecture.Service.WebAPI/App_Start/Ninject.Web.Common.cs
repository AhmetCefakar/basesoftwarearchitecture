[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(BaseSoftwareArchitecture.Service.WebAPI.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(BaseSoftwareArchitecture.Service.WebAPI.App_Start.NinjectWebCommon), "Stop")]

namespace BaseSoftwareArchitecture.Service.WebAPI.App_Start
{
    using System;
    using System.Web;
	using System.Web.Http;
	using BaseSoftwareArchitecture.BusinessLocigLayer.DependencyResolvers.Ninject;
	using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;
	using Ninject.Web.Common.WebHost;
	using Ninject.Web.WebApi;

	// Read this post: https://www.infoworld.com/article/3191648/application-development/how-to-implement-di-in-webapi-using-ninject.html
	public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();
				GlobalConfiguration.Configuration.DependencyResolver = new NinjectDependencyResolver(kernel);
				RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
			// Buradaki tan�mlamalar Ninject ��z�mlemelerini aya�a kald�rmak i�in eklenen s�n�flardan olu�uyor
			// 'BusinessModule' gibi 'Ninject Module' s�n�flar� her Presentation projesinde tekrar tekrar yaz�lmas�n 
			// tek bir yerde yap�lan tan�mlamalar kullan�ls�n diye BLL katman�ndan �ekilerek kullan�l�yor.
			kernel.Load(
				new BusinessModule(), 
				new ValidationModule(),
				new AutoMapperModule());
        }        
    }
}