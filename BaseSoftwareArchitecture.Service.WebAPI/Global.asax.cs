﻿using BaseSoftwareArchitecture.BusinessLocigLayer.DependencyResolvers.Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace BaseSoftwareArchitecture.Service.WebAPI
{
	public class WebApiApplication : System.Web.HttpApplication
	{
		protected void Application_Start()
		{
			GlobalConfiguration.Configure(WebApiConfig.Register);

			// Varsayılan Controller'ın değişmesiyle ilgili eklenen kod.
			//ControllerBuilder.Current.SetControllerFactory(new NinjectControllerFactory(new BusinessModule()));
		}
	}

}
