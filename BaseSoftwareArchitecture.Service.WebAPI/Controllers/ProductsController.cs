﻿using BaseSoftwareArchitecture.BusinessLocigLayer.Abstract;
using BaseSoftwareArchitecture.Entity.Concrete.Northwind.BaseType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BaseSoftwareArchitecture.Service.WebAPI.Controllers
{
	/// <summary>
	/// Product entity sınıfı için web.API uygulaması üstündeki sunumu
	/// </summary>
	public class ProductsController : ApiController
	{
		private IProductService _productService;
		public ProductsController(IProductService productService)
		{
			_productService = productService;
		}


		public List<Products> Get()
		{
			return _productService.GetAll();
		}

	}
}
