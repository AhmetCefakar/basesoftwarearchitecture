﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

using System.Text;

using System.Threading;
using System.Threading.Tasks;

using System.Security.Principal;
using BaseSoftwareArchitecture.BusinessLocigLayer.Abstract;
using BaseSoftwareArchitecture.BusinessLocigLayer.DependencyResolvers.Ninject;
using BaseSoftwareArchitecture.Entity.Concrete.Northwind.BaseType;

namespace BaseSoftwareArchitecture.Service.WebAPI.MessageHandlers
{
	public class AuthenticationHandler : DelegatingHandler
	{
		protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
		{
			try
			{
				// 'tokenValues[0]': Kullanıcı adı
				// 'tokenValues[1]': Kullanıcı şifre
				string token = request.Headers.GetValues("Authorization").FirstOrDefault();

				if (token != null)
				{
					byte[] data = Convert.FromBase64String(token);
					string decodingString = Encoding.UTF8.GetString(data);
					string[] tokenValues = decodingString.Split(':');

					// Burada 'InstanceFactory' sınıfı vasıtasıyle 
					IUserService userService = InstanceFactory.GetInstance<IUserService>();

					User currentUser = userService.GetByUserNameAndPassword(tokenValues[0], tokenValues[1]);
					if (currentUser != null)
					{
						// .Net mimarisindeki Authantication yapılarının kullanılması(hem Presentation Layer hem BLL katmanı için)
						//Todo: Buradaki identity çözümü biraz karışık ve User tablosunda Role tablosuyla olan ilişki atanmamış durumda, buraları düzelt.
						IPrincipal principle = new GenericPrincipal(
							new GenericIdentity(tokenValues[0]), 
							userService.GetUserRoles(currentUser).Select(q => q.RoleName).ToArray()
							);
						HttpContext.Current.User = principle; // Web.API katmanında authantication kullanabilmek için eklendi
						Thread.CurrentPrincipal = principle; // BLL katmanında authantication kullanabilmek için eklendi

					}
				}
				
			}
			catch (Exception ex)
			{

			}

			return base.SendAsync(request, cancellationToken);
		}
	}
}