﻿using BaseSoftwareArchitecture.DataAccessLayer.Abstract.GenericRepository.NorthwindDal.ForOrm;
using BaseSoftwareArchitecture.DataAccessLayer.Concrete.ORM.NHibernate.Helper;
using BaseSoftwareArchitecture.DataAccessLayer.Concrete.ORM.NHibernate.RepositoryBase;
using BaseSoftwareArchitecture.Entity.Concrete.Northwind.BaseType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSoftwareArchitecture.DataAccessLayer.Concrete.ORM.NHibernate.NorthwindDal
{
	// ToDo: Bu kısımdaki 'NHibernateHelper' nesnesi, EF için olduğu gibi dışarıdan alınmadan da yazılabilir.
	public class NhCategoryDal : NhRepositoryBase<Categories>, ICategoiesDalForOrm
	{
		public NhCategoryDal(NHibernateHelper nHibernateHelper):base(nHibernateHelper)
		{
			
		}
	}
}
