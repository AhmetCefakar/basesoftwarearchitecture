﻿using BaseSoftwareArchitecture.DataAccessLayer.Abstract.GenericRepository.NorthwindDal.ForMicroOrm;
using BaseSoftwareArchitecture.DataAccessLayer.Abstract.GenericRepository.NorthwindDal.ForOrm;
using BaseSoftwareArchitecture.DataAccessLayer.Concrete.ORM.NHibernate.Helper;
using BaseSoftwareArchitecture.DataAccessLayer.Concrete.ORM.NHibernate.RepositoryBase;
using BaseSoftwareArchitecture.Entity.Concrete.Northwind.BaseType;
using BaseSoftwareArchitecture.Entity.Concrete.Northwind.ComplexType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSoftwareArchitecture.DataAccessLayer.Concrete.ORM.NHibernate.NorthwindDal
{
	public class NhUserDal : NhRepositoryBase<User>, IUserDalForOrm, IUserDalForMicroOrm
	{
		public NhUserDal(NHibernateHelper nHibernateHelper) : base(nHibernateHelper)
		{

		}

		public List<UserRolesComplexType> GetUserRoles(User user)
		{
			IEnumerable<UserRolesComplexType> result;
			using (var session = _nHibernateHelper.OpenSession())
			{
				result = from r in session.Query<Role>()
						 join ur in session.Query<UserRole>()
						 on r.Id equals ur.RoleId
						 where ur.UserId == user.Id
						 select new UserRolesComplexType { RoleName = r.Name };
			}
			return result.ToList();
		}
	}
}
