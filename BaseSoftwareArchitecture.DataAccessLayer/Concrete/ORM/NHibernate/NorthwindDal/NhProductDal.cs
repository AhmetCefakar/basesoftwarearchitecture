﻿using BaseSoftwareArchitecture.DataAccessLayer.Abstract.GenericRepository.NorthwindDal.ForOrm;
using BaseSoftwareArchitecture.DataAccessLayer.Concrete.ORM.NHibernate.Helper;
using BaseSoftwareArchitecture.DataAccessLayer.Concrete.ORM.NHibernate.RepositoryBase;
using BaseSoftwareArchitecture.Entity.Concrete.Northwind.BaseType;
using BaseSoftwareArchitecture.Entity.Concrete.Northwind.ComplexType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSoftwareArchitecture.DataAccessLayer.Concrete.ORM.NHibernate.NorthwindDal
{
	// ToDo: Bu kısımdaki 'NHibernateHelper' nesnesi, EF için olduğu gibi dışarıdan alınmadan da yazılabilir.
	public class NhProductDal : NhRepositoryBase<Products>, IProductDalForOrm
	{
		//public NHibernateHelper _nHibernateHelper { get; set; }
		public NhProductDal(NHibernateHelper nHibernateHelper) : base(nHibernateHelper)
		{

		}

		public List<ProductDetail> GetProductDetails()
		{
			IEnumerable<ProductDetail> result;
			using (var session = _nHibernateHelper.OpenSession())
			{
				result = from p in session.Query<Products>()
						 join c in session.Query<Categories>()
						 on p.CategoryID equals c.CategoryID
						 select new ProductDetail
						 {
							 ProductID = p.ProductID,
							 ProductName = p.ProductName,
							 CategoryName = c.CategoryName
						 };
			}

			return result.ToList();
		}
	}
}
