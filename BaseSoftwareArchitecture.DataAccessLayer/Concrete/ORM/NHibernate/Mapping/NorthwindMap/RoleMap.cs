﻿using BaseSoftwareArchitecture.Entity.Concrete.Northwind.BaseType;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSoftwareArchitecture.DataAccessLayer.Concrete.ORM.NHibernate.Mapping.NorthwindMap
{
	internal class RoleMap: ClassMap<Role>
	{
		public RoleMap()
		{
			Table(@"[Role]");
			Schema(@"[dbo]");
			LazyLoad();
			Id(q => q.Id);
			
			Map(q => q.Name).Column("Name");
		}
	}
}
