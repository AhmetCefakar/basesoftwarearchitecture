﻿using BaseSoftwareArchitecture.Entity.Concrete.Northwind.BaseType;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSoftwareArchitecture.DataAccessLayer.Concrete.ORM.NHibernate.Mapping.NorthwindMap
{
	internal class CategoryMap: ClassMap<Categories>
	{
		public CategoryMap()
		{
			Table(@"[Categories]");
			Schema(@"[dbo]");
			LazyLoad();
			Id(q => q.CategoryID);

			//Map(q => q.CategoryID).Column("CategoryID"); // Not: NHibernate'te 'Id' kolonu tanımlandıktan sanra tekrar 'map' edilince hata veriyor
			Map(q => q.CategoryName).Column("CategoryName");
		}
	}
}
