﻿using BaseSoftwareArchitecture.Entity.Concrete.Northwind.BaseType;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSoftwareArchitecture.DataAccessLayer.Concrete.ORM.NHibernate.Mapping.NorthwindMap
{
	internal class UserMap : ClassMap<User>
	{
		public UserMap()
		{
			Table(@"[User]");
			Schema(@"[dbo]");
			LazyLoad();
			Id(q => q.Id);
			
			Map(q => q.UserName).Column("UserName");
			Map(q => q.Password).Column("Password");
			Map(q => q.FirstName).Column("FirstName");
			Map(q => q.LastName).Column("LastName");
			Map(q => q.Email).Column("Email");
		}
	}
}
