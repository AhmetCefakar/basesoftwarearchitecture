﻿using BaseSoftwareArchitecture.Entity.Concrete.Northwind.BaseType;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSoftwareArchitecture.DataAccessLayer.Concrete.ORM.NHibernate.Mapping.NorthwindMap
{
	internal class ProdutMap: ClassMap<Products>
	{
		public ProdutMap()
		{
			Table(@"[Products]");
			Schema(@"[dbo]");
			LazyLoad();
			Id(q => q.ProductID);

			Map(q => q.CategoryID).Column("CategoryID");
			Map(q => q.ProductName).Column("ProductName");
			Map(q => q.QuantityPerUnit).Column("QuantityPerUnit");
			Map(q => q.UnitPrice).Column("UnitPrice");
		}
	}
}
