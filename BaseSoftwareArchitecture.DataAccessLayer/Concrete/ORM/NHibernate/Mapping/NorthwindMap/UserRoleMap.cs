﻿using BaseSoftwareArchitecture.Entity.Concrete.Northwind.BaseType;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSoftwareArchitecture.DataAccessLayer.Concrete.ORM.NHibernate.Mapping.NorthwindMap
{
	internal class UserRoleMap : ClassMap<UserRole>
	{
		public UserRoleMap()
		{
			Table(@"[UserRole]");
			Schema(@"[dbo]");
			LazyLoad();
			Id(q => q.Id);
			
			Map(q => q.UserId).Column("UserId");
			Map(q => q.RoleId).Column("RoleId");
		}
	}
}
