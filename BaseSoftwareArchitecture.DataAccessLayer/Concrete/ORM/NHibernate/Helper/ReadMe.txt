﻿NHibernate teknolojisiyle birlikte eğer birden çok veritabanı kullanılacaksa 'NorthwindHelper' benzeri 
dosyalar üretmek ve bu dosyaları 'NhCategoryDal' vb. sınıfların kurucularına ilgili veritabanı için
özel olarak yazılan sınıf inject edilir, hepsi bu kadar işte.