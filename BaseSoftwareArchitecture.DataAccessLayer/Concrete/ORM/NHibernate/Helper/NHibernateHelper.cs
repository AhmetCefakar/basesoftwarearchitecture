﻿using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSoftwareArchitecture.DataAccessLayer.Concrete.ORM.NHibernate.Helper
{
	/// <summary>
	/// NHibernate 'Session' mantığı üzerinden çalışıyor.(EF'nin 'Context' mantığı ile çalışması gibi)
	/// </summary>
	public abstract class NHibernateHelper : IDisposable
	{
		private static ISessionFactory _sessionFactory;
		public virtual ISessionFactory SessionFactory
		{
			get
			{
				return _sessionFactory ?? (_sessionFactory = InitializeFactory());
			}
		}

		// Session oluşturmak için kullanılan Metod
		protected abstract ISessionFactory InitializeFactory();

		/// <summary>
		/// NHibernate için session açan metod
		/// </summary>
		/// <returns></returns>
		public virtual ISession OpenSession()
		{
			return SessionFactory.OpenSession();
		}

		public void Dispose()
		{
			GC.SuppressFinalize(this);
		}
	}
}
