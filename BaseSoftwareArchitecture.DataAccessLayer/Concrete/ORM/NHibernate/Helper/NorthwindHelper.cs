﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;

namespace BaseSoftwareArchitecture.DataAccessLayer.Concrete.ORM.NHibernate.Helper
{
	/// <summary>
	/// NHibernate için veritabanı bağlantısının ayarlamalarının yapıldığı sınıf
	/// </summary>
	public class NorthwindHelper : NHibernateHelper
	{
		protected override ISessionFactory InitializeFactory()
		{
			// Harici bir paket kullanılarak(FluentNHibernate) ile veritabanı bağlantı ayarları yapılıyor(configuration işlemleri)
			return Fluently.Configure()
				.Database(MsSqlConfiguration.MsSql2012.ConnectionString(con => con.FromConnectionStringWithKey("NorthwindContext")))
				.Mappings(map => map.FluentMappings.AddFromAssembly(Assembly.GetExecutingAssembly())) // Bu satır ile bu proje içinde varolan bütün mapping sınıfları ekleniyor
				.BuildSessionFactory();
		}
	}
}
