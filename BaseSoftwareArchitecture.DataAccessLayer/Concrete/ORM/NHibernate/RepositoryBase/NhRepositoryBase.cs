﻿using BaseSoftwareArchitecture.DataAccessLayer.Abstract.GenericRepository;
using BaseSoftwareArchitecture.DataAccessLayer.Concrete.ORM.NHibernate.Helper;
using BaseSoftwareArchitecture.Entity.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BaseSoftwareArchitecture.DataAccessLayer.Concrete.ORM.NHibernate.RepositoryBase
{
	/// <summary>
	/// Bu sınıf dışarıdan gelen 'NHibernateHelper' tipi ile çalışıyor
	/// </summary>
	/// <typeparam name="TEntity"></typeparam>
	public class NhRepositoryBase<TEntity> : IGenericRepositoryForOrm<TEntity>
		where TEntity : class, IEntity, new()
	{
		protected NHibernateHelper _nHibernateHelper;
		public NhRepositoryBase(NHibernateHelper nHibernateHelper)
		{
			_nHibernateHelper = nHibernateHelper;
		}

		public TEntity Add(TEntity entity)
		{
			using (var session = _nHibernateHelper.OpenSession())
			{
				session.Save(entity);
			}
			return entity;
		}

		public TEntity Update(TEntity entity)
		{
			using (var session = _nHibernateHelper.OpenSession())
			{
				session.Update(entity);
			}
			return entity;
		}

		public void Delete(TEntity entity)
		{
			using (var session = _nHibernateHelper.OpenSession())
			{
				session.Delete(entity);
			}
		}

		//public TEntity GetById(int id)
		//{
		//	TEntity entity;
		//	using (var session = _nHibernateHelper.OpenSession())
		//	{
		//		entity = session.Query<TEntity>().FirstOrDefault(q => q.Equals(id));
		//	}
		//	return entity;
		//}

		public List<TEntity> GetAll()
		{
			List<TEntity> entityList;
			using (var session = _nHibernateHelper.OpenSession())
			{
				entityList = session.Query<TEntity>().ToList();
			}
			return entityList;
		}

		public TEntity Get(Expression<Func<TEntity, bool>> filter)
		{
			TEntity entity;
			using (var session = _nHibernateHelper.OpenSession())
			{
				entity = session.Query<TEntity>().SingleOrDefault(filter);
			}
			return entity;
		}
		
		public List<TEntity> GetList(Expression<Func<TEntity, bool>> filter)
		{
			List<TEntity> entityList;
			using (var session = _nHibernateHelper.OpenSession())
			{
				entityList = session.Query<TEntity>().Where(filter).ToList();
			}
			return entityList;
		}

		public IQueryable<TEntity> Table()
		{
			return _nHibernateHelper.OpenSession().Query<TEntity>();
		}
	}
}
