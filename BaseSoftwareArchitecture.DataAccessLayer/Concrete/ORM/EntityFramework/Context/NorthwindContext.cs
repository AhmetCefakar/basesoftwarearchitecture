﻿using BaseSoftwareArchitecture.DataAccessLayer.Concrete.ORM.EntityFramework.Mapping.NorthwindMap;
using BaseSoftwareArchitecture.Entity.Concrete.Northwind.BaseType;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace BaseSoftwareArchitecture.DataAccessLayer.Concrete.ORM.EntityFramework.Context
{
	public class NorthwindContext : DbContext
	{
		public NorthwindContext()
		{
			Database.SetInitializer<NorthwindContext>(null); // Bu kod veritabanına kod tarafından müdahalleyi engeller.
		}

		// S.O.S: Kendine not EF'te eğer bir entity sınıfına mapping eklemez isen tablo isimlerini varsayılan olarak 's, es' ekleriyle çoğul yapıyor!
		public DbSet<Products> Products { get; set; }
		public DbSet<Categories> Categories { get; set; }
		public DbSet<User> User { get; set; }
		public DbSet<Role> Role { get; set; }
		public DbSet<UserRole> UserRole { get; set; }
		
		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			// Her bir sınıfın mapping sınıfı ekleniyor
			modelBuilder.Configurations.Add(new ProductMap());
			modelBuilder.Configurations.Add(new CategoryMap());
			modelBuilder.Configurations.Add(new UserMap());
			modelBuilder.Configurations.Add(new RoleMap());
			modelBuilder.Configurations.Add(new UserRoleMap());

			base.OnModelCreating(modelBuilder);
		}
	}
}
