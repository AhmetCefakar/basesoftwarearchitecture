﻿using BaseSoftwareArchitecture.DataAccessLayer.Abstract.GenericRepository.NorthwindDal.ForMicroOrm;
using BaseSoftwareArchitecture.DataAccessLayer.Abstract.GenericRepository.NorthwindDal.ForOrm;
using BaseSoftwareArchitecture.DataAccessLayer.Concrete.ORM.EntityFramework.RepositoryBase;
using BaseSoftwareArchitecture.Entity.Concrete.Northwind.BaseType;
using BaseSoftwareArchitecture.Entity.Concrete.Northwind.ComplexType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSoftwareArchitecture.DataAccessLayer.Concrete.ORM.EntityFramework.NorthwindDal
{
	public class EfUserDal : EfNorthwindRepository<User>, IUserDalForOrm, IUserDalForMicroOrm
	{
		public List<UserRolesComplexType> GetUserRoles(User user)
		{
			IEnumerable<UserRolesComplexType> result = from r in _northwindContext.Role
													   join ur in _northwindContext.UserRole
													   on r.Id equals ur.RoleId
													   where ur.UserId == user.Id
													   select new UserRolesComplexType { RoleName = r.Name };

			return result.ToList();
		}
	}
}
