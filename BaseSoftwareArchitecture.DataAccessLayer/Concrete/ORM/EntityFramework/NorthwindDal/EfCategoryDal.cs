﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using BaseSoftwareArchitecture.Entity.Concrete.Northwind.BaseType;
using BaseSoftwareArchitecture.DataAccessLayer.Abstract.GenericRepository.NorthwindDal.ForOrm;
using BaseSoftwareArchitecture.DataAccessLayer.Abstract.GenericRepository.NorthwindDal.ForMicroOrm;
using BaseSoftwareArchitecture.DataAccessLayer.Concrete.ORM.EntityFramework.RepositoryBase;

namespace BaseSoftwareArchitecture.DataAccessLayer.Concrete.ORM.EntityFramework.NorthwindDal
{
	public class EfCategoryDal : EfNorthwindRepository<Categories>, ICategoiesDalForOrm, ICategoiesDalForMicroOrm
	{

	}
}
