﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using BaseSoftwareArchitecture.Entity.Concrete.Northwind.BaseType;
using BaseSoftwareArchitecture.Entity.Concrete.Northwind.ComplexType;
using BaseSoftwareArchitecture.DataAccessLayer.Abstract.GenericRepository.NorthwindDal.ForOrm;
using BaseSoftwareArchitecture.DataAccessLayer.Abstract.GenericRepository.NorthwindDal.ForMicroOrm;
using BaseSoftwareArchitecture.DataAccessLayer.Concrete.ORM.EntityFramework.RepositoryBase;

namespace BaseSoftwareArchitecture.DataAccessLayer.Concrete.ORM.EntityFramework.NorthwindDal
{
	// ToDo: 'NorthwindContext' sınıfından nesne üretimi kalıtım alınan bir üst sınıfta(EfNorthwindRepository) yapılıyor
	public class EfProductDal : EfNorthwindRepository<Products>, IProductDalForOrm, IProductDalForMicroOrm
	{
		public List<ProductDetail> GetProductDetails()
		{
			//using (NorthwindContext context = new NorthwindContext()) // Bu satırda context 'new'lemesi yapılıyordu, yapı bir üst sınıfta singleton kullanımı ile değiştirildi.
			//{

			// '_northwindContext' değeri inherit alınan 'EfNorthwindRepository<Products>' sınıfından geliyor
			var result = from p in _northwindContext.Products
						 join c in _northwindContext.Categories
						 on p.CategoryID equals c.CategoryID
						 select new ProductDetail
						 {
							 ProductID = p.ProductID,
							 ProductName = p.ProductName,
							 CategoryName = c.CategoryName
						 };

			return result.ToList();
			//}
		}
	}
}
