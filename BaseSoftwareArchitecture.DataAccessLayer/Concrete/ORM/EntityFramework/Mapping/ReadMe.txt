﻿Bu "Mapping" klasörü altında 'EntityFramework' ORM teknolojisiyle ilgili veritabanı nesnelerinin ayarlamak için map sınıfları tutuluyor.

Not: Bu dosyalar eklenmeden de çalışılabilir. Eğer veritabanındaki isimlerden başka isimlendirme kullanılacaksa ya da özel ayarlar yapılacaksa kullanılmalıdır.