﻿using BaseSoftwareArchitecture.Entity.Concrete.Northwind.BaseType;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSoftwareArchitecture.DataAccessLayer.Concrete.ORM.EntityFramework.Mapping.NorthwindMap
{
	/// <summary>
	/// 'Product' entity'si için mapping sınıfı
	/// </summary>
	internal class ProductMap: EntityTypeConfiguration<Products>
	{
		public ProductMap()
		{
			ToTable(@"Products", @"dbo");
			HasKey(q => q.ProductID);

			// Kolon adlarının ayarlanmasını sağlayan kodlar
			Property(q => q.ProductID).HasColumnName("ProductID");
			Property(q => q.CategoryID).HasColumnName("CategoryID");
			Property(q => q.ProductName).HasColumnName("ProductName");
			Property(q => q.QuantityPerUnit).HasColumnName("QuantityPerUnit");
			Property(q => q.UnitPrice).HasColumnName("UnitPrice");
		}
	
	}
}
