﻿using BaseSoftwareArchitecture.Entity.Concrete.Northwind.BaseType;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSoftwareArchitecture.DataAccessLayer.Concrete.ORM.EntityFramework.Mapping.NorthwindMap
{
	/// <summary>
	/// 'Product' entity'si için mapping sınıfı
	/// </summary>
	internal class UserMap : EntityTypeConfiguration<User>
	{
		public UserMap()
		{
			ToTable(@"User", @"dbo");
			HasKey(q => q.Id);
			
			// Kolon adlarının ayarlanmasını sağlayan kodlar
			Property(q => q.Id).HasColumnName("Id");
			Property(q => q.UserName).HasColumnName("UserName");
			Property(q => q.Password).HasColumnName("Password");
			Property(q => q.FirstName).HasColumnName("FirstName");
			Property(q => q.LastName).HasColumnName("LastName");
			Property(q => q.Email).HasColumnName("Email");
		}
	
	}
}
