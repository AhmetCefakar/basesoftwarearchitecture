﻿using BaseSoftwareArchitecture.Entity.Concrete.Northwind.BaseType;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSoftwareArchitecture.DataAccessLayer.Concrete.ORM.EntityFramework.Mapping.NorthwindMap
{
	/// <summary>
	/// 'Product' entity'si için mapping sınıfı
	/// </summary>
	internal class UserRoleMap : EntityTypeConfiguration<UserRole>
	{
		public UserRoleMap()
		{
			ToTable(@"UserRole", @"dbo");
			HasKey(q => q.Id);
			
			// Kolon adlarının ayarlanmasını sağlayan kodlar
			Property(q => q.Id).HasColumnName("Id");
			Property(q => q.UserId).HasColumnName("UserId");
			Property(q => q.RoleId).HasColumnName("RoleId");
		}
	
	}
}
