﻿using BaseSoftwareArchitecture.Entity.Concrete.Northwind.BaseType;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSoftwareArchitecture.DataAccessLayer.Concrete.ORM.EntityFramework.Mapping.NorthwindMap
{
	/// <summary>
	/// 'Product' entity'si için mapping sınıfı
	/// </summary>
	internal class CategoryMap : EntityTypeConfiguration<Categories>
	{
		public CategoryMap()
		{
			ToTable(@"Categories", @"dbo");
			HasKey(q => q.CategoryID);

			// Kolon adlarının ayarlanmasını sağlayan kodlar
			Property(q => q.CategoryID).HasColumnName("CategoryID");
			Property(q => q.CategoryName).HasColumnName("CategoryName");
		}
	
	}
}
