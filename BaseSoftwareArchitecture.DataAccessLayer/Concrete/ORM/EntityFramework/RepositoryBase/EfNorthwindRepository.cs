﻿using BaseSoftwareArchitecture.DataAccessLayer.Concrete.ORM.EntityFramework.Context;
using BaseSoftwareArchitecture.DataAccessLayer.Concrete.ORM.EntityFramework.RepositoryBase;
using BaseSoftwareArchitecture.Entity.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSoftwareArchitecture.DataAccessLayer.Concrete.ORM.EntityFramework.RepositoryBase
{
	// Bu sınıf 'Context' değerini dışarıdan almadan çalışabilmek için dahil edildi.
	// Birden fazla veritabanı ile çalışılıyorsa bu sınıfa benzeyen aynı mantıkta bir kalıtım hiyerarşisi ile 
	// 'Ef<DBName>Repository' şeklinde isimlendirme standartında sınıflar üretilebilir. 
	// Bu şekilde contex değerinin diğer katmanlardan alınmasınınn önüne geçilmiş olunuyor.
	public class EfNorthwindRepository<TEntity> : EfRepositoryBase<TEntity>
		where TEntity : class, IEntity, new()
	{
		// 'Thread Safety Singleton using Double Check Locking' ile context değeri belirleniyor
		private static readonly object padlock = new object(); // context nesnesinden tek instance üretmek için kullanılıyor
		public EfNorthwindRepository()
		{
			if (_context == null)
			{
				lock (padlock)
				{
					if (_context == null)
					{
						_context = new NorthwindContext();
					}
				}
			}
		}

		// Alt sınıflara 'NorthwindContext' türünde bir context vermek için kullanılan property
		protected NorthwindContext _northwindContext
		{
			get
			{
				return (_context as NorthwindContext);
			}
		}

	}
}
