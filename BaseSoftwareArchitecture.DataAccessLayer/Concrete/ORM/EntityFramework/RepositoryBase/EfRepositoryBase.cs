﻿using BaseSoftwareArchitecture.DataAccessLayer.Abstract.GenericRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Data.Entity;
using BaseSoftwareArchitecture.Entity.Abstract;

namespace BaseSoftwareArchitecture.DataAccessLayer.Concrete.ORM.EntityFramework.RepositoryBase
{
	/// <summary>
	/// Bu sınıftaki metodlar bütün entity türleri için ortak olan işlemleri yapar.
	/// </summary>
	/// <typeparam name="TEntity"></typeparam>
	/// <typeparam name="TContext"></typeparam>
	public class EfRepositoryBase<TEntity> : IGenericRepositoryForOrm<TEntity>
		where TEntity : class, IEntity, new()
		//where TContext : DbContext, new()
	{
		protected DbContext _context;
		//public EfRepositoryBase(DbContext context)
		//{
		//	_context = context;
		//}

		public TEntity Add(TEntity entity)
		{
			var addedEntity = _context.Entry(entity);
			addedEntity.State = EntityState.Added;
			_context.SaveChanges();
			return entity;
		}

		public TEntity Update(TEntity entity)
		{
			var updatedEntity = _context.Entry(entity);
			updatedEntity.State = EntityState.Modified;
			_context.SaveChanges();
			return entity;
		}

		public void Delete(TEntity entity)
		{
			var deletedEntity = _context.Entry(entity);
			deletedEntity.State = EntityState.Deleted;
			_context.SaveChanges();
		}


		//public TEntity GetById(int id)
		//{
		//	return _context.Set<TEntity>().FirstOrDefault(q => q.Equals(id));
		//}

		public List<TEntity> GetAll()
		{
			return _context.Set<TEntity>().ToList();
		}

		public TEntity Get(Expression<Func<TEntity, bool>> filter)
		{
			return _context.Set<TEntity>().SingleOrDefault(filter);
		}

		public List<TEntity> GetList(Expression<Func<TEntity, bool>> filter)
		{
			return _context.Set<TEntity>().Where(filter).ToList();
		}

		public IQueryable<TEntity> Table()
		{
			return _context.Set<TEntity>();
		}

	}
}
