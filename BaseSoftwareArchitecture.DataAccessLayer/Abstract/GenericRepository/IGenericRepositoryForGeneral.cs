﻿using BaseSoftwareArchitecture.Entity.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSoftwareArchitecture.DataAccessLayer.Abstract.GenericRepository
{
	/// <summary>
	/// This Interface Deglerate Generic Repository Interface For ORM and MicroORM tools
	/// Bu interface hem ORM hem de MicroORM tool'larında bulunacak olan metodları içerir
	/// </summary>
	public interface IGenericRepositoryForGeneral<TEntity> 
		where TEntity : class, IEntity, new()
	{
		// ORM-MicroORM için ortak metodlar metodlar
		//TEntity GetById(int id); // Bu kullanım sadece 'Id' değeri int türünden olan tablolardan veri çekerken kullanılır
		List<TEntity> GetAll();
		TEntity Add(TEntity entity);
		TEntity Update(TEntity entity);
		void Delete(TEntity entity);
	}
}
