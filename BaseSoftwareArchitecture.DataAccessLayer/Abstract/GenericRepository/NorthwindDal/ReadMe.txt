﻿'BaseSoftwareArchitecture.DataAccessLayer.Abstract.GenericRepository.NorthwindDal' name-space alanında ilgili veritabanına ait olarak kullanılan 
yaklaşıma göre(Orm-MicroOrm) linq Expression ifadeleri ya kullanılabiliyor ya da kullanılamıyor bundan dolayı burada kullanılan interface'ler
'ForMicroOrm' ve 'ForOrm' olarak ayrıldı, bu şekilde uygulama mimarisinde aynı anda hem 1 tane ORM tool hemde 1 tane MicroORM tool kullanılabilir.
