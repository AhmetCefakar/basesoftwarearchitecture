﻿using BaseSoftwareArchitecture.Entity.Concrete.Northwind.BaseType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSoftwareArchitecture.DataAccessLayer.Abstract.GenericRepository.NorthwindDal.ForMicroOrm
{
	public interface IUserDalForMicroOrm : IGenericRepositoryForMicroOrm<User>
	{

	}
}
