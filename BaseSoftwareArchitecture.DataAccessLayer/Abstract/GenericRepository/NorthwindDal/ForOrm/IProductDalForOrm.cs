﻿using BaseSoftwareArchitecture.Entity.Concrete.Northwind.BaseType;
using BaseSoftwareArchitecture.Entity.Concrete.Northwind.ComplexType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSoftwareArchitecture.DataAccessLayer.Abstract.GenericRepository.NorthwindDal.ForOrm
{
	/// <summary>
	/// Bu sınıf, 'Generic Repository' deseninde her bir tablonun kendine has metodlarını barındırma mantığı çerçeverinde 
	/// 'Product' entity'si için tanımlandı
	/// </summary>
	public interface IProductDalForOrm : IGenericRepositoryForOrm<Products>
	{
		List<ProductDetail> GetProductDetails();
	}
}
