﻿using BaseSoftwareArchitecture.Entity.Concrete.Northwind.BaseType;
using BaseSoftwareArchitecture.Entity.Concrete.Northwind.ComplexType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSoftwareArchitecture.DataAccessLayer.Abstract.GenericRepository.NorthwindDal.ForOrm
{
	public interface IUserDalForOrm : IGenericRepositoryForOrm<User>
	{
		List<UserRolesComplexType> GetUserRoles(User user);
	}
}
