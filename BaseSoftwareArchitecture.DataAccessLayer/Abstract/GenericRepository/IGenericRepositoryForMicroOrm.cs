﻿using BaseSoftwareArchitecture.Entity.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseSoftwareArchitecture.DataAccessLayer.Abstract.GenericRepository
{
	/// <summary>
	/// This Interface Deglerate Generic Repository Interface For MicroORM tools, but it's methods declare yet.
	/// </summary>
	public  interface IGenericRepositoryForMicroOrm<TEntity> : IGenericRepositoryForGeneral<TEntity>
		where TEntity : class, IEntity, new()
	{

	}
}
