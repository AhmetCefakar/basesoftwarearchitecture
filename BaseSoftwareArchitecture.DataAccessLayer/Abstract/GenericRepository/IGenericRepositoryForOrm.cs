﻿using BaseSoftwareArchitecture.Entity.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BaseSoftwareArchitecture.DataAccessLayer.Abstract.GenericRepository
{
	/// <summary>
	/// This Interface Deglerate Generic Repository Interface For ORM tools
	/// </summary>
	/// <typeparam name="TEntity"></typeparam>
	public interface IGenericRepositoryForOrm<TEntity> : IGenericRepositoryForGeneral<TEntity>
		where TEntity : class, IEntity, new()
	{
		// Sadece Orm tool'larında kullanıla bilecek metodlar
		List<TEntity> GetList(Expression<Func<TEntity, bool>> filter);
		TEntity Get(Expression<Func<TEntity, bool>> filter);
		IQueryable<TEntity> Table();
	}
}
